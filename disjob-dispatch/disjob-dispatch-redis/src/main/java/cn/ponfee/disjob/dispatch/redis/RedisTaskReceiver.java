/* __________              _____                                                *\
** \______   \____   _____/ ____\____   ____    Copyright (c) 2017-2023 Ponfee  **
**  |     ___/  _ \ /    \   __\/ __ \_/ __ \   http://www.ponfee.cn            **
**  |    |  (  <_> )   |  \  | \  ___/\  ___/   Apache License Version 2.0      **
**  |____|   \____/|___|  /__|  \___  >\___  >  http://www.apache.org/licenses/ **
**                      \/          \/     \/                                   **
\*                                                                              */

package cn.ponfee.disjob.dispatch.redis;

import cn.ponfee.disjob.common.base.SingletonClassConstraint;
import cn.ponfee.disjob.common.base.TimingWheel;
import cn.ponfee.disjob.common.concurrent.AbstractHeartbeatThread;
import cn.ponfee.disjob.common.spring.RedisKeyRenewal;
import cn.ponfee.disjob.core.base.JobConstants;
import cn.ponfee.disjob.core.base.Worker;
import cn.ponfee.disjob.dispatch.ExecuteTaskParam;
import cn.ponfee.disjob.dispatch.TaskReceiver;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.connection.ReturnType;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.script.RedisScript;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import static cn.ponfee.disjob.common.spring.RedisTemplateUtils.evalScript;
import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * Task receiver based redis.
 *
 * <p>
 * <pre>Redis list queue: {@code
 * lrange list_queue 0 -1
 *
 * rpush list_queue a b c d e f g
 * lrange list_queue 0 -1
 *
 * lrange list_queue 0 2
 * lrange list_queue 0 -1
 *
 * ltrim  list_queue 3 -1
 * lrange list_queue 0 -1
 * }</pre>
 *
 * @author Ponfee
 */
public class RedisTaskReceiver extends TaskReceiver {
    private static final Logger LOG = LoggerFactory.getLogger(TaskReceiver.class);

    /**
     * List Batch pop lua script
     *
     * <pre>{@code
     *   // 1、获取[0 ~ n-1]之间的元素
     *   lrange(key, 0, n-1)
     *
     *   // 2、保留[n ~ -1]之间的元素
     *   ltrim(key, n, -1)
     * }</pre>
     */
    private static final RedisScript<List> BATCH_POP_SCRIPT = RedisScript.of(
        "local ret = redis.call('lrange', KEYS[1], 0, ARGV[1]-1); \n" +
        "redis.call('ltrim', KEYS[1], ARGV[1], -1);               \n" +
        "return ret;                                              \n" ,
        List.class
    );

    /**
     * List left pop batch size
     */
    private static final byte[] LIST_POP_BATCH_SIZE_BYTES = Integer.toString(JobConstants.PROCESS_BATCH_SIZE).getBytes(UTF_8);

    private final RedisTemplate<String, String> redisTemplate;
    private final GroupedWorker gropedWorker;
    private final AtomicBoolean started = new AtomicBoolean(false);
    private final ReceiveHeartbeatThread receiveHeartbeatThread;

    public RedisTaskReceiver(Worker.Current currentWorker,
                             TimingWheel<ExecuteTaskParam> timingWheel,
                             RedisTemplate<String, String> redisTemplate) {
        super(currentWorker, timingWheel);
        SingletonClassConstraint.constrain(this);

        this.redisTemplate = redisTemplate;
        this.gropedWorker = new GroupedWorker(currentWorker);
        this.receiveHeartbeatThread = new ReceiveHeartbeatThread(1000);
    }

    @Override
    public void start() {
        if (!started.compareAndSet(false, true)) {
            LOG.warn("Repeat call start method.");
            return;
        }
        this.receiveHeartbeatThread.start();
    }

    @Override
    public void stop() {
        if (!started.compareAndSet(true, false)) {
            LOG.warn("Repeat call stop method.");
            return;
        }
        this.receiveHeartbeatThread.close();
    }

    private class ReceiveHeartbeatThread extends AbstractHeartbeatThread {
        private ReceiveHeartbeatThread(long heartbeatPeriodMs) {
            super(heartbeatPeriodMs);
        }

        @Override
        protected boolean heartbeat() {
            List<byte[]> received = evalScript(redisTemplate, BATCH_POP_SCRIPT, ReturnType.MULTI, 1, gropedWorker.keysAndArgs);
            gropedWorker.redisKeyRenewal.renewIfNecessary();
            if (CollectionUtils.isEmpty(received)) {
                return true;
            }
            for (byte[] bytes : received) {
                RedisTaskReceiver.this.receive(ExecuteTaskParam.deserialize(bytes));
            }
            return received.size() < JobConstants.PROCESS_BATCH_SIZE;
        }
    }

    private class GroupedWorker {
        private final byte[][] keysAndArgs;
        private final RedisKeyRenewal redisKeyRenewal;

        private GroupedWorker(Worker.Current worker) {
            byte[] key = RedisTaskDispatchingUtils.buildDispatchTasksKey(worker).getBytes();
            this.keysAndArgs = new byte[][]{key, LIST_POP_BATCH_SIZE_BYTES};
            this.redisKeyRenewal = new RedisKeyRenewal(redisTemplate, key);
        }
    }

}
